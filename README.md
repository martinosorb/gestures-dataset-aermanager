
# Managing the IBM Gestures dataset with aermanager

[`aermanager`](https://gitlab.com/synsense/aermanager) is a small library for
managing `aedat` event-based recordings in Python in a fast way.

This is a specific script that shows how to use aermanager with the [IBM Gestures
dataset](https://www.research.ibm.com/dvsgesture/), since this dataset has a
rather custom way of providing its data and labels.

There is also a script that tests a spiking network written in [sinabs](https://sinabs.ai)
on the Gestures test set.

## Using `convert.py`

```
Usage: convert.py [-h] [--fraction FRACTION] [--dt DT] source

Convert IBM Gestures aedat files to hdf5.

positional arguments:
  source               Folder where the dataset is located.

optional arguments:
  -h, --help           show this help message and exit
  --fraction FRACTION  Dataset fraction ('train' or 'test')
  --dt DT              Time interval (in ms) for slicing the dataset into
                       frames. If not provided, only save the full gestures.
```

Here, `source` is the DvsGesture folder, which should contain the `trials_to_train.txt`,
`trials_to_test.txt` files, and all the `csv` and `aedat` files with the data.

By default, this script creates a folder `full_gesture_dataset` containing
sub-folders, one per each label. In each sub-folder there are all the repetitions
of that gesture, gathered from the various aedat files.

If `dt` is also provided, another folder will be created, containing the "sliced"
gestures, each with `dt` milliseconds of data. All the files, in both datasets,
are in the usual aermanager format, and contain the label, spike train, a single
accumulated frame and some other metadata.

For more information, see the code itself, which is heavily commented for clarity.

## The `show_results` notebook
This notebook contains a visualisation of the resulting  frames, to check
that the results are correct.

## Testing a spiking network

The file `test_spiking_net.py` is a demonstration on how the dataset can be
used with a "neuromorphic" convolutional neural network with Sinabs.

The pretrained network is provided, and only testing is performed in this file.
