from aermanager.aerparser import load_events_from_file
from aermanager.preprocess import accumulate_frames, slice_by_time
from aermanager.dataset_generator import save_to_hdf5, save_to_dataset
from pathlib import Path
import numpy as np
from tqdm import tqdm
import argparse

# - ARGUMENTS MANAGEMENT
parser = argparse.ArgumentParser(description='Convert IBM Gestures aedat files to hdf5.')
parser.add_argument('source', type=str, help="Folder where the dataset is located.")
parser.add_argument(
    '--fraction', default="test", type=str,
    help="Dataset fraction ('train' or 'test')"
)
parser.add_argument(
    '--dt', default=None, type=int,
    help="Time interval (in ms) for slicing the dataset into frames. \
    If not provided, only save the full gestures.",
)
parser.add_argument(
    '--overlap', default=0, type=int,
    help="Overlap time windows by this amount (ms)."
)
args = parser.parse_args()

SOURCE = Path(args.source)
PHASE = args.fraction
SLICE_DT = args.dt  # ms; set to None if you don't want a sliced dataset
FULL_GESTURE_SAVE_DIR = Path("full_gesture_dataset") / PHASE
folder_name = f"gesture_dataset_{SLICE_DT}ms"
if args.overlap:
    folder_name += f"_overlap{args.overlap}ms"
SLICED_GESTURE_SAVE_DIR = Path(folder_name) / PHASE


# First, load the txt files which tell us which files are train or test.
file = SOURCE / f"trials_to_{PHASE}.txt"
with open(file, "r") as f:
    # split into lines and remove empty ones
    aedat_list = [line for line in f.read().splitlines() if line]

# - LOOP OVER EACH AEDAT FILE
for recording in tqdm(aedat_list):
    aedat_file = SOURCE / recording
    # read the aedat file with the beginning and end of each gesture
    csv_file = str(aedat_file).replace(".aedat", "_labels.csv")
    # three cols: label, start in µs, end in µs
    csv_data = np.loadtxt(csv_file, delimiter=",", skiprows=1, dtype=int)
    # NOTE that we use labels from 0 to 10 for the 11 classes
    labels = csv_data[:, 0] - 1
    # a sequence of gesture start, gesture stop, gesture start, gesture stop, ...
    start_stop_sequence = csv_data[:, 1:].ravel()
    # check it's sorted
    assert np.all(start_stop_sequence[:-1] <= start_stop_sequence[1:]), \
        f"File {recording} has overlapping labels. Please check its annotations file."

    # Load a single .aedat file into memory, with all its events.
    shape, spikes = load_events_from_file(aedat_file, parser="ibm")
    # get the event indices corresponding to the time delimiters
    split_indices = np.searchsorted(spikes["t"], start_stop_sequence)
    # split the events into non-gesture, gesture, non-gesture, gesture, ...
    spike_groups = np.split(spikes, split_indices)
    # now filter out the beginning, end, and bits in between valid gestures.
    gesture_recordings = spike_groups[1:-1:2]
    # double check that event groups are within the start,end range.
    for i in range(len(csv_data)):
        label, start, end = csv_data[i]
        gesture_spikes = gesture_recordings[i]
        assert gesture_spikes["t"][0] >= start
        assert gesture_spikes["t"][-1] < end

    # - GENERATE DATASET WITH FULL GESTURES
    # In this dataset, we discard the data between a gesture and the next,
    # and we save separately each gesture. We will get h5 files with a messy
    # frame, and with all the spikes corresponding to a gesture (usually 4-7 seconds).

    # accumulate frame with the whole 4 to 7 seconds gesture. We don't need it
    # but it keeps the format of the hdf5 files uniform if we have it.
    bins = (range(shape[0] + 1), range(shape[1] + 1))
    gesture_frames = accumulate_frames(
        gesture_recordings,
        bins_y=bins[0], bins_x=bins[1]
    )
    # loop over each gesture. NOTE that this is usually 12, not 11, because
    # one of the gestures (n. 7) is normally repeated twice in the recordings.
    for i, gesture_spikes in enumerate(gesture_recordings):
        # determine file name. original file name + start time
        basename = aedat_file.stem + "_" + str(gesture_spikes["t"][0])
        # they are put in separate folders according to label, like many vision datasets.
        file_name = FULL_GESTURE_SAVE_DIR / str(labels[i]) / (basename + ".h5")
        file_name.parent.mkdir(parents=True, exist_ok=True)
        # now save.
        save_to_hdf5(
            xytp=gesture_spikes,
            frame=gesture_frames[i],
            label=labels[i],
            bins=bins,
            file_name=file_name,
            compression=None
        )

    # - GENERATE DATASET OF SLICED FRAMES FOR EACH GESTURE OF EACH FILE
        if SLICE_DT is not None:
            # Slice the data the data at every SLICE_DT milliseconds
            sliced_events = slice_by_time(gesture_spikes, time_window=SLICE_DT * 1000,
                                          overlap=args.overlap * 1000)
            # prepare frames for each 50 ms slice
            frames = accumulate_frames(
                sliced_events,
                bins_y=bins[0], bins_x=bins[1]
            )
            # save all the files in a folder
            folder = SLICED_GESTURE_SAVE_DIR / basename
            folder.mkdir(parents=True, exist_ok=True)
            save_to_dataset(
                sliced_xytp=sliced_events,
                frames=frames,
                labels=[labels[i]] * len(frames),
                bins=bins,
                destination_path=folder,
                compression=None,
            )
