import torch
from tqdm import tqdm
import numpy as np

from model import SpeckNetA
from aermanager.datasets import SpikeTrainDataset
from sinabs.from_torch import from_model


# - DATASET
SCALE_INPUT = 1.0  # multiply inputs by a factor?
MAX_MS = 1000  # limit each gesture to 1 second of length

# the data will be presented to the network as a raster with 1000 µs bins
ds_test_spk = SpikeTrainDataset("../full_gesture_dataset/test", dt=1000)
dl_test = torch.utils.data.DataLoader(
    ds_test_spk, batch_size=1, num_workers=1, shuffle=True
)


# - MODEL
LOAD = 'SpeckNetA_augmented.pth'
# instantiate the pretrained model
model = SpeckNetA(final_relu=True, n_out=11, final_flatten=False)
model.load_state_dict(torch.load(LOAD), strict=True)
# convert to spiking
net = from_model(model.seq, (2, 128, 128), synops=True)
net = net.cuda()
net.eval()


# - TESTING
N_LABELS = 11
confusion_matrix = np.zeros((N_LABELS, N_LABELS), dtype=int)
pbar = tqdm(dl_test)
correct = 0
powers = []

for i, (frames, target) in enumerate(pbar):
    frames = frames.squeeze()[:MAX_MS].cuda() * SCALE_INPUT
    target = target.int().numpy()
    net.reset_states()

    result = net(frames)
    predicted_label = torch.max(result.sum(0), axis=0)[1].item()

    confusion_matrix[predicted_label, target.item()] += 1
    correct += predicted_label == target.item()

    powers.append(net.synops_counter.get_total_power_use())
    pbar.set_postfix(
        rolling_accuracy=correct / (i + 1),
        ms=len(frames),
        power=f"{np.mean(powers):.3f} mW"
    )

report = f"Model: {LOAD}, input scale: {SCALE_INPUT}, max ms: {MAX_MS}\n"
report += f"Accuracy: {correct / (i + 1)}\n"
report += f"Avg. consumption (mW): {np.mean(powers)}\n"
report += "Confusion matrix:\n"
report += str(confusion_matrix)

print(report)
with open("report.txt", "w") as f:
    f.write(report)
